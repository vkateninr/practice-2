using System;

public interface ICarInterior
{
    public void ShowCarInterior();
}

public interface ICarColor
{
    public void ShowCarColor();
}

public class OldFashionedCarInterior : ICarInterior
{
    public void ShowCarInterior()
    {
        Console.WriteLine("Старомодный салон автомобиля");
    }
}

public class BrandNewCarInterior : ICarInterior
{
    public void ShowCarInterior()
    {
        Console.WriteLine("Совершенно новый салон автомобиля");
    }
}

public class BlackCarColor : ICarColor
{
    public void ShowCarColor()
    {
        Console.WriteLine("Чёрный цвет автомобиля");
    }
}

public class WhiteCarColor : ICarColor
{
    public void ShowCarColor()
    {
        Console.WriteLine("Белый цвет автомобиля");
    }
}

public interface ICarFactory
{
    public ICarInterior CreateCarInterior();
    public ICarColor CreateCarColor();
}

public class FirstCarFactory : ICarFactory
{
    public ICarInterior CreateCarInterior()
    {
        return new BrandNewCarInterior();
    }
    public ICarColor CreateCarColor()
    {
        return new WhiteCarColor();
    }
}

public class SecondCarFactory : ICarFactory
{
    public ICarInterior CreateCarInterior()
    {
        return new OldFashionedCarInterior();
    }

    public ICarColor CreateCarColor()
    {
        return new BlackCarColor();
    }
}

class Car
{
    private ICarInterior _carInterior;
    private ICarColor _carColor;

    public Car(ICarFactory carFactory)
    {
        _carInterior = carFactory.CreateCarInterior();
        _carColor = carFactory.CreateCarColor();
    }

    public void ShowElements()
    {
        _carInterior.ShowCarInterior();
        _carColor.ShowCarColor();
    }
}

interface ICarIterator
{
    bool HasNext();
    Car Next();
}

interface ICarNumerable
{
    ICarIterator CreateIterator();
    int Count { get; }
    Car this[int index] { get; }
}

class Customer
{
    public void ShowCars(Showroom showroom)
    {
        ICarIterator iterator = showroom.CreateIterator();
        while (iterator.HasNext())
        {
            Car car = iterator.Next();
            car.ShowElements();
        }
    }
}

class Showroom : ICarNumerable
{
    private Car[] cars;

    public Showroom()
    {
        cars = new Car[]
        {
            new Car(new FirstCarFactory()),
            new Car(new SecondCarFactory())
        };
    }

    public int Count => cars.Length;

    public Car this[int index] => cars[index];

    public ICarIterator CreateIterator()
    {
        return new ShowroomIterator(this);
    }
}

class ShowroomIterator : ICarIterator
{
    private ICarNumerable _cars;
    int index = 0;

    public ShowroomIterator(ICarNumerable cars)
    {
        _cars = cars;
    }

    public bool HasNext()
    {
        return index < _cars.Count;
    }

    public Car Next()
    {
        return _cars[index++];
    }
}

class Program
{
    static void Main()
    {
        Showroom showroom = new Showroom();
        Customer customer = new Customer();
        customer.ShowCars(showroom);
    }
}